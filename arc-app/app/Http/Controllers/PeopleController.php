<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;

class PeopleController extends Controller
{

    public function showAllPeople()
    {
        $results = array('results' => People::all());
        return response()->json($results);
    }

    public function showOnePeople($id)
    {
        $results = array('results' => People::find($id));
        return response()->json($results);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'password' => 'required|alpha_num',
            'id_number' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'mobile' => 'required|max:13'
        ]);

        $people = People::create($request->all());

        return response()->json($people, 201);
    }

    public function update($id, Request $request)
    {
        $people = People::findOrFail($id);
        $people->update($request->all());

        return response()->json($people, 200);
    }

    public function delete($id)
    {
        People::findOrFail($id)->delete();
        return response('Deleted Successfully', 204);
    }
}