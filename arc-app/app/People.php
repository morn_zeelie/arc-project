<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'password', 'id_number', 'email', 'mobile' 
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}