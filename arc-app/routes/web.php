<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    

    // this will generate a new api token when you authenticate your users
    $router->post('auth/login', ['uses' => 'AuthController@authenticate']);

    // middleware protection for your route paths
    $router->group(['middleware' => 'jwt.auth'], function() use ($router) {
        $router->get('users', function() {
            $users = \App\User::all();
            return response()->json($users);
        });

        $router->get('people',  ['uses' => 'PeopleController@showAllPeople']);

        $router->get('people/{id}', ['uses' => 'PeopleController@showOnePeople']);

        $router->post('people', ['uses' => 'PeopleController@create']);

        $router->delete('people/{id}', ['uses' => 'PeopleController@delete']);

        $router->put('people/{id}', ['uses' => 'PeopleController@update']);
    });
});
