# API Design decisions

I have decided to go with the light weight Lumen framework which is actually a Laravel based framework meant for Api's
It's a framework on a diet and it is quick and easy to work with. [Lumen Framework](https://lumen.laravel.com)

I've implement a token based system which also uses the Authentication system of Lumen/Laravel. Once a user authenticates
it will generate a token for them which they will use until it expires. When it expires they would have to generate a new token.
So basically authenticate and get a token, do your business and then token expires. When you come back you need to do it again.
It's similar to Oauth.

Lumen has middleware protection so you can't bypass the token system in anyway as long as your routes runs through the middleware.

# Interacting with the API

You can use a Rest client or use the curl commands below.

### Generate a token for your user

```curl -i -X POST \
   -H "Content-Type:application/x-www-form-urlencoded" \
   -d "email=irutherford@example.org" \
   -d "password=secret" \
 'http://localhost:8000/api/v1/auth/login'```


### Retrieve people with the token generated. Replace token if needed.

 ```curl -i -X GET \
 'http://localhost:8000/api/v1/people?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjEsImlhdCI6MTUzOTUwNDI2MiwiZXhwIjoxNTM5NTA3ODYyfQ.rKlUKv7CEgpHI-XbBEyWorbfWELqBpaKhc12QYxXRPg'```


### create more people 

```curl -i -X POST \
   -H "Content-Type:application/x-www-form-urlencoded" \
   -d "firstname='Morne53'" \
   -d "lastname='Zeelie53'" \
   -d "password=123456" \
   -d "id_number=12156546978" \
   -d "email=holla22@gmail.com" \
   -d "mobile=023456789" \
 'http://localhost:8000/api/v1/people?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIsImlhdCI6MTUzOTUwNjM3MSwiZXhwIjoxNTM5NTA5OTcxfQ.VWfye8EQVx6YPrkCaiWxAhvo9PvwsvmMk3_CTBgLP84'```


### delete a person, you would need to change the number 2 to a different user id. 

 ```curl -i -X DELETE \
 'http://localhost:8000/api/v1/people/2?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIsImlhdCI6MTUzOTUwNjM3MSwiZXhwIjoxNTM5NTA5OTcxfQ.VWfye8EQVx6YPrkCaiWxAhvo9PvwsvmMk3_CTBgLP84'```


### get one person only via id

 ```curl -i -X GET \
 'http://localhost:8000/api/v1/people/1?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIsImlhdCI6MTUzOTUwNjM3MSwiZXhwIjoxNTM5NTA5OTcxfQ.VWfye8EQVx6YPrkCaiWxAhvo9PvwsvmMk3_CTBgLP84'```

###  update person

 ```curl -i -X PUT \
   -H "Content-Type:application/x-www-form-urlencoded" \
   -d "email=random2@gmail.com" \
   -d "mobile=060583593" \
   -d "token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIsImlhdCI6MTUzOTUxMTIwNywiZXhwIjoxNTM5NTE0ODA3fQ.TtxwcYIALGF0km20Kx-5xSUD8xjKkbu5ARC8ZUW9OQc" \
 'http://localhost:8000/api/v1/people/1''```


 # Contract for API

 #People
* People object
```
{
  id: integer,
  firstname: string,
  lastname: string,
  password: integer,
  id_number: integer,
  email: string,
  mobile: integer,
  created_at: datetime(iso 8601)
  updated_at: datetime(iso 8601)
}
```
**GET /people**
----
  Returns all people in the system.
* **URL Params**  
  None
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Success Response:**  
* **Code:** 200  
  **Content:**  
```
{
  people: [
           {
            "id": integer,
            "firstname": "string'",
            "lastname": "string",
            "password": "string",
            "id_number": "integer",
            "email": "string",
            "mobile": "integer",
            "created_at": datetime(iso 8601),
            "updated_at": datetime(iso 8601)
            },
         ]
}
```

**GET /people/:id**
----
  Returns the specified user.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  token:`<Token>`
* **Headers**  
  Content-Type: application/json  
* **Success Response:** 
* **Code:** 200  
  **Content:**  `{ <people_object> }` 
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ error : "People doesn't exist" }`  
  OR  
  * **Code:** 401  
  **Content:** `{ error : error : "Token not provided, you are unauthorized to make this request." }`


**POST /people**
----
  Creates a new Person and returns the new object.
* **URL Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Data Params**  
```
  {
    firstname: string,
    lastname: string,
    password: string,
    id_number: integer,
    email: string,
    mobile: integer,
}
```
* **Success Response:**  
* **Code:** 200  
  **Content:**  `{ <user_object> }` 

**PUT /people/:id**
----
  Updates Person and returns object.
* **URL Params**  
   *Required:* `id=[integer]`
* **Headers**  
  Content-Type: application/json  
* **Data Params**  
    token:`<Token>`
```
  {
    firstname: string,
    lastname: string,
    password: string,
    id_number: integer,
    email: string,
    mobile: integer,
}
```
* **Success Response:**  
* **Code:** 200  
  **Content:**  `{ <user_object> }` 

**DELETE /people/:id**
----
  Deletes the specified user.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  token:`<Token>`
* **Headers**  
  Content-Type: application/json  
* **Success Response:** 
  * **Code:** 204 
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ error : "Person doesn't exist" }`  
  OR  
  * **Code:** 401  
  **Content:** `{ error : error : "Token not provided, you are unauthorized to make this request." }`