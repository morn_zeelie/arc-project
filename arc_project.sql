-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2018 at 05:46 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arc_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(8, '2018_10_13_151030_create_people_table', 3),
(11, '2018_10_14_062321_create_users_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'x4hnJenQyJbxZ715f7u1uDAJ1tiAO78JMAspvY3n', 'http://localhost', 1, 0, 0, '2018-10-13 11:00:28', '2018-10-13 11:00:28'),
(2, NULL, 'Laravel Password Grant Client', 'qin0fchWQzOwEUDlpJx2m68UCWoCYAPPoecGDXyn', 'http://localhost', 0, 1, 0, '2018-10-13 11:00:28', '2018-10-13 11:00:28'),
(3, 1, 'ULhYa8s6DahI@mail.com', 'RSvXKyRoTu3e4LMMiNNnPOKBe8MSDcLaYW5ekAMM', 'http://localhost/auth/callback', 0, 0, 0, '2018-10-13 12:04:05', '2018-10-13 12:04:05');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-10-13 11:00:28', '2018-10-13 11:00:28');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_number` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `password`, `id_number`, `email`, `mobile`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kameron', 'Waelchi', '$2y$10$PcQma07YAz3nnortqHemSe.FG7EdoWiY5PV.k6a3sRwTyBnPTRtie', 5471050557720632, 'erich.hahn@example.org', 782, 'IDSv8bKBX4', '2018-10-14 07:19:15', '2018-10-14 07:19:15'),
(2, 'Lina', 'Bechtelar', '$2y$10$Mo40WKg5wC4o6bQqy64wd.KWMtldp.tW5t1v/MTgvxe9yPM0fdVAS', 4982357677017012, 'irutherford@example.org', 0, 'oHffj2FYk6', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(3, 'Evalyn', 'Hoppe', '$2y$10$uqDoF8wkrtirG7SUkfXlhe9KR0QaCOLCbz/r4RqUBjGOgM6/E3/Ga', 5342091404670932, 'ondricka.toby@example.org', 486761, 'xWAfJtxgd1', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(4, 'Osborne', 'Conn', '$2y$10$CP0GAtZHOuilo2P2Vjf7GeMI7yFUy42AzO2dUUX5wBBH1K.Src2MK', 5487328362931335, 'lang.arnold@example.org', 202966, 'tZ2axKFl73', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(5, 'Carli', 'Ortiz', '$2y$10$XNynBfTlxOGJ6rJIErfnheIYea1DizLHd66w9XwsWSa8qd4RGUkjK', 4485543271144680, 'monahan.rickie@example.org', 352, 'GyfC7nQg4r', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(6, 'Tavares', 'VonRueden', '$2y$10$M26HaNrACIreurr0jTQriuAsENkMHmkVKIMI64U/kygz.md.9LSLG', 4929945162039339, 'emorar@example.com', 901, 'urfjpGQbjX', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(7, 'Adrianna', 'Feil', '$2y$10$y72ZFt3AFAstFehDSWhsoe0O6/yNZncRs3CTuQW4rbTW37xLsmjjq', 5421283744063373, 'kenneth21@example.org', 1, 'vRNwbFXUtt', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(8, 'Vidal', 'Renner', '$2y$10$wuhy.CtCXWhpRFyXDGh1CeLfpC6m0W9HRETB3RFP9m5cfA2uVz6Y6', 4716771868198998, 'kareem53@example.com', 276, '3rFKesyz1g', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(9, 'Megane', 'Nader', '$2y$10$FX36j95x9rRvB8gINFpMNuaT4tvnA1pxNxj/m75qztjR3H8pWj79S', 2632631855145037, 'erik09@example.net', 548338, 'znwSLeM6ve', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(10, 'Wilhelmine', 'Carter', '$2y$10$ewFVovcvb3luhMaT/Zqh8e3Yfo/CsdLkLwvieUyvfzGCWcT78oxzO', 6011000713965485, 'donnell65@example.com', 352, 'zU5Oy7ek3A', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(11, 'Merlin', 'Hackett', '$2y$10$PHnkyaWeFo709c9eANgogu7R1ly.vObu0f8e3mCCapysh7N2mJlaK', 2512926442319250, 'ybogan@example.net', 440, 'VY12kFu05m', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(12, 'Avery', 'Rath', '$2y$10$kA1c6ry/wF.yNqqPJe061.L3slHrDoLULrlNjvYbororyKMOAYa9e', 2221014578909491, 'ekohler@example.net', 782, '2M1eTnEQcA', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(13, 'Maci', 'Cronin', '$2y$10$am5e4vJMaaJZ83RUr9AoxurXoOh4npyNVf47o2Pbuu5qzX0lZjBq2', 4716781008443378, 'thelma27@example.org', 1818, 'AxUVE5rDFS', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(14, 'Velva', 'Kuhic', '$2y$10$HtHzQTVYduIqZEWz/cGxpeM0W98R/BHHUgqfBWcFbMAeDE4x.d86m', 6011685456769993, 'waters.armani@example.net', 15623907562, 'KPONxI4WK2', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(15, 'Uriel', 'Fadel', '$2y$10$sc/n2QS3XJ7NNimDYj4hSeUg9KR5idLbMrxltB3AaP6BV/E8uLLF2', 4548582612928752, 'levi.bayer@example.net', 880, 'HHjH6pgsWV', '2018-10-14 07:19:16', '2018-10-14 07:19:16'),
(16, 'Felix', 'Schamberger', '$2y$10$VRoRRyMLKpYJLqesxXEgE.20ipttZY3XMmLI4vSTCMxYlSWhkzlLK', 4916846037260107, 'patsy.schultz@example.org', 1, 'kpsHGCk1Yc', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(17, 'Max', 'Lynch', '$2y$10$pQe/d4P3ZHN9exGUH/8oheTQGGqXZvLY23z.OplyqSHUYV7z8knce', 6011691553532318, 'konopelski.katherine@example.org', 1, 'grtDoVEu3h', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(18, 'Julianne', 'Johnson', '$2y$10$t6HnU68N2KOZ/BSKWxCrYOSGdt5slg/x6mbeHtOGTShR9iHpTsVy6', 4556949595706480, 'zboncak.merlin@example.org', 1, 'ziDYh43l1h', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(19, 'Justen', 'Schaefer', '$2y$10$Iz.tdzMG/WaFIy9Hk9GpHOnxozqFkFFXWXQbGg5MgnGVoHyzUWSVe', 5277864839831033, 'zstreich@example.net', 1, '58eGvyReZe', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(20, 'Delphine', 'Rath', '$2y$10$8vVKRptZei169opqc2408.idM7O0hsDva/GUHVeHkilz8mBKO/J6G', 5203029355868137, 'ansley85@example.net', 0, 'sw10bo7tsf', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(21, 'Hank', 'Pfeffer', '$2y$10$3W8k/dEHxXVOWATaFXii0.5Vpr57pZLM1YMWguIwVrlHxaF8Ojkxe', 4485341587989314, 'kharber@example.com', 1, '4Nonq9O1ei', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(22, 'Frieda', 'Hayes', '$2y$10$NAOHZXy3Bl/Ku41lq/9ybOuge6ALZ4YdYDYL2Db4BusboG2QL0Fmq', 5261848314687588, 'dwight79@example.net', 364, 'qZYHwBjdx9', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(23, 'Wava', 'Schultz', '$2y$10$onUjvC/Ta9NIgvB9PM7FIep/wGi.0x.3LWExN8UbkiaI01yjl3yt2', 4929843941492723, 'brandt.roob@example.net', 0, 'YQusOUuD6y', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(24, 'Cecilia', 'Rosenbaum', '$2y$10$naOi0ZCx5l1CXDfm/.ZDj.tnIJ.3ZROipP8p7dqp6OAhpzNNSyYUe', 5360107438147882, 'sabina14@example.net', 1, 'x0AVGpZyFl', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(25, 'Pearlie', 'Ratke', '$2y$10$U4ly9neHuKCMc8WV8ZjU1ufH9nyLHukzr9jc5UmfnvW0/dO5g2tiu', 4916112091504462, 'doyle.micheal@example.org', 1, 'RUMwEStr9G', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(26, 'Norbert', 'Rogahn', '$2y$10$g9D/xRVgb31cWyOH3aCYGunGxrsLc2YeodyRLSvrbRGL.goGHZqEK', 4556751950632, 'dustin.hansen@example.com', 224, 'yuIgQ9mWIC', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(27, 'Juanita', 'Reichel', '$2y$10$/9UFyQP7R4KG16brhisyLOEj8CXh8lGJConlHPpVSWE1UXPo0FTXm', 4916527439383008, 'kassulke.lorenz@example.com', 1524, '6jzm7oumMx', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(28, 'Lorna', 'Lebsack', '$2y$10$.i91sRXIFzf/VDEMoQj5E...GPfAg.QzJnE4yjzFA2JGSDA05whL.', 2720797705540512, 'pamela.stracke@example.com', 1, 'xnZ4EQZOAH', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(29, 'Kareem', 'Orn', '$2y$10$I5NwRL2Mj7PaKeE0CuEOsuXkBRgYs98A4vpQ4ir4VfBAUnjY.P/56', 4716103388629114, 'mayert.ezequiel@example.org', 382560, 'jwHKDojDco', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(30, 'Karley', 'Koepp', '$2y$10$hiLsK.OUOJuUGC1Bs6YPFesBFNniERTUuuT5GkRfVxLEQQxlshhBG', 5209714246179257, 'sylvia.hessel@example.com', 0, 'AJwmEy8xnx', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(31, 'Thea', 'Runolfsson', '$2y$10$16Idj1Bwd4KXIq4jZytdNuQH6huoy3b/TPMbqBGxv5nwXeL1b8xq2', 4929451995032203, 'ukeeling@example.net', 650484, 'VNAUskZhYl', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(32, 'Brycen', 'Miller', '$2y$10$wu1Q22T8kWHmd3tECKMF8escie9RqJUYckAdZBU.CXZ.4YOT8/HIu', 2597565704589636, 'dschaefer@example.net', 813214, 'Iax4zpSIah', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(33, 'Brown', 'Wyman', '$2y$10$.wDbfJksV.7mDJ.ayH1dHetZj4AYgNlpWYHu/jlfIuI1m3V5BsLZi', 4929723359961981, 'lelah13@example.com', 1, 'uLJRFE5VAy', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(34, 'Laurel', 'Wintheiser', '$2y$10$BX7DkscixNW98t7CJAhLneZ90kgHTeVjN7Pb8NowJEsCuNWyMUZ4a', 5241292999321541, 'abner26@example.org', 0, 'OwYi34NMWV', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(35, 'Emmalee', 'Bahringer', '$2y$10$vMkP2E7MzPAhDdDok61QNO57unVI0i7/.AIWUCs0hqXG3.7mYomPW', 2720360355895197, 'paucek.retta@example.org', 1, 'YEu2GJWOx0', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(36, 'Margaret', 'Hamill', '$2y$10$simKG9vjCpYgKKFYKPeuYeGxpSttY1CQMBHjR9WPst2nkI0q2qede', 4556162149765578, 'emard.freddy@example.org', 1728, 'cPSDN2P2lh', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(37, 'Osborne', 'Robel', '$2y$10$iW.x0ptx4l7IQKZDFW6DrO4qnBySqSiqlobx6vvK5mqH.hAs25rVm', 6011996672097352, 'trever.spencer@example.com', 357, 'xxOEwkDKP2', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(38, 'Nelda', 'Stanton', '$2y$10$0U.89dUcdf/VjHw.xUp09eOfRU3LOrS7Xip53Vu7rfJdhe2ZBuJlq', 5429551240890321, 'tyrel25@example.com', 0, 'VUfuiN0Jw7', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(39, 'Aryanna', 'Dooley', '$2y$10$YVg6KwzkxAR6juMcL2VR4ukJRymdTUPOqWuuXDc2cewUJr94Gl6g.', 2445173290540289, 'leila.berge@example.com', 824455, 'KytEdJEiDr', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(40, 'Otis', 'Dickens', '$2y$10$vvRYWhCNcrsAVbTKdzPgx.dnvY5PK63t5hv44QANtVIjwbRnyAMAK', 4929331460013070, 'nwolf@example.net', 1, 'lk61TbbXMN', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(41, 'Kamille', 'Wuckert', '$2y$10$UZeeJv3z3nhKsH.JZ7O81OUJNE/.RTubSkIrihZ0SzyOhSbUlJ1sW', 6011954457640557, 'ispencer@example.com', 13135021287, 'P2SR4hokys', '2018-10-14 07:19:17', '2018-10-14 07:19:17'),
(42, 'Andy', 'Marks', '$2y$10$pJZk2SBdiQ0.FH6YUnO9OunlFCtUqsY1EQRiJGKl4SH1Dgv1d5dE6', 6011746572489385, 'xhyatt@example.org', 706342, 'iQkIEAZPUL', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(43, 'Mckenna', 'Quigley', '$2y$10$oCJQH7xGBxpH/BTUNYrSoOwYr0..l/Vc5oz7hS5.z.BsnZts2YCp6', 4024007170083504, 'whegmann@example.org', 223, 'VyJXSOslqE', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(44, 'Frederick', 'Feest', '$2y$10$Qhom8EGfPgM9gt0BgN3U4ujBdHn4wavXvwdagw/9oNJ50EDGZsMv2', 4485427136972, 'zwyman@example.org', 1, 'ExYo3YRCqy', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(45, 'Shirley', 'Labadie', '$2y$10$HSn2srG41PgveiQ.n3QsveODeObKN8tRZ3JpMULUwsqlLGHYIUCDK', 5517877966048866, 'ressie21@example.org', 1, 'ux2dAFhNJS', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(46, 'Assunta', 'Harber', '$2y$10$ragiMNjPQ6USyqsm7RUW0OxCwszj2IIBs2F3vDnK4CF7S5iiNuQkG', 4916179381795431, 'bernier.rafael@example.net', 385602, 'u2KNmYNQQh', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(47, 'Margie', 'Heidenreich', '$2y$10$EIdo5bLueB/EpZXthXx3yOxuAv0oQUj5w/isUW3z5gD8dhOfkjxxi', 6011571647689029, 'isabell49@example.com', 1, 'egdXJzwpdj', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(48, 'Dayne', 'Medhurst', '$2y$10$eE29UX5MBBIIXbRR.c4d0edIAQs3H5GzevmDmaJ3pDRBSjAxjQjau', 4916478856473536, 'white.jeanie@example.net', 1, 'Cu5k4dyiPZ', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(49, 'Jeromy', 'Marvin', '$2y$10$HMfEHXQr.SBhOQ2x4/.iReJoO9k1hPPQDTc10iMadEeprCaVKH2fm', 4485549956262457, 'amelie32@example.com', 0, 'p3TxKnE2fV', '2018-10-14 07:19:18', '2018-10-14 07:19:18'),
(50, 'Wilma', 'Hammes', '$2y$10$xC96LlyJ1BIoGphfNdMcdOLv3C7b8JcBtGXe3n9HCsuyZzEcA.Qhm', 4024007144817219, 'kokon@example.com', 1, 'vkbVOwQePf', '2018-10-14 07:19:18', '2018-10-14 07:19:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
