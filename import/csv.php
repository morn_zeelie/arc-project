<?php
/**
  * This is my custom CSV import class.
  *
  * I'm just catering this for once off users table but can be more dynamic.
  *
  * @author  Morne Zeelie
  * @since 1.0
  */
class CSV_IMPORT
{
    // I love constructors :-) it's so nice man.
    public function __construct ($host,$db,$user,$pass,$csv_path) 
    {
        $conn = $this->db_connection($host,$db,$user,$pass);
        // lets get the file and get data back nicely.
        $aData = $this->get_csv($csv_path);
        // lets import data yay.
        $this->import($conn,$csv,$aData);

        // just some debugging code below.
        echo "<pre>";
        print_r($aData);
        echo "</pre>";

    }

    private function db_connection($host,$db,$user,$pass)
    {

        /**
         If you want your column to support storing characters lying outside the BMP (and you usually want to), such as emoji, use "utf8mb4".
        */
        $charset = 'utf8mb4'; 

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            $pdo = new PDO($dsn, $user, $pass, $options);

            return $pdo;
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    private function get_csv($fileName)
    {
        $row = 1;
        $users = [];
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            $data = fgetcsv($handle, 1000, ",");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                
                $num = count($data);

                $row++; // Don't want the first line
                for ($c=0; $c < $num; $c++) {
                    // lets create a pretty users array
                    // lets md5 passwords, could of added salt if I wanted or bcrypt
                    $users[$row] =  [
                        'firstname' => $data[0],
                        'lastname' => $data[1],
                        'password' => md5($data[2]), 
                        'id_number' => $data[3],
                        'email' => $data[4],
                        'mobile' => $data[5]
                    ];
                }
            }
            fclose($handle);

            return $users;
        }
        return "Sorry no file found!";
    }

    private function import($conn,$csv,$aData)   
    {
        $dataNow = date("Y-m-d H:i:s");
        try 
        {
            if(!empty($aData) || is_array($aData))
                foreach($aData as $userData)
                {
                    // set the PDO error mode to exception
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    // lets do some binding, it's just safe.
                    // this was originally a Users table, but seeing that Lumin has it's own authenication
                    // I decided to just use this data as persons data for demonstration purposes.
                    $sth = $conn->prepare("INSERT INTO people (firstname, lastname, password, id_number, email, mobile, created_at, updated_at)
                    VALUES (:firstname, :lastname, :password, :id_number, :email, :mobile, :created_at, :updated_at)");

                    $sth->bindParam(':firstname', $userData['firstname'], PDO::PARAM_STR,30);
                    $sth->bindParam(':lastname', $userData['lastname'], PDO::PARAM_STR, 30);
                    $sth->bindParam(':password', $userData['password'], PDO::PARAM_STR, 30);
                    $sth->bindParam(':id_number', $userData['id_number'], PDO::PARAM_INT, 80);
                    $sth->bindParam(':email', $userData['email'], PDO::PARAM_STR, 100);
                    $sth->bindParam(':mobile', $userData['mobile'], PDO::PARAM_STR, 20);
                    $sth->bindParam(':created_at', $dataNow, PDO::PARAM_STR, 100);
                    $sth->bindParam(':updated_at',  $dataNow, PDO::PARAM_STR, 100);
                    $sth->execute();

                }
            
            echo "New records imported successfully";
        }
        catch(PDOException $e)
        {
            echo $sql . "<br>" . $e->getMessage();
        }

        $conn = null;
    }
}

/**
 * Edit the values below to fit your database.
 * You can just instantiate the CSV_IMPORT class like below as I'm using a constructor which makes the code look
 * better in the end.
 */
$import = new CSV_IMPORT('127.0.0.1','arc_project','root','','C:\morne-code\arc-project\import\users.csv'); ///var/www/html/arc-code/import/users.csv


?>