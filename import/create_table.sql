CREATE DATABASE arc_project;

USE arc_project;

CREATE TABLE Users (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
password VARCHAR(30) NOT NULL,
id_number INT(30) NOT NULL,
email VARCHAR(100),
mobile VARCHAR(20),
reg_date TIMESTAMP
);


CREATE USER 'arc' IDENTIFIED BY 'arc123';

/** 
    Grant user access for localhost 
*/
GRANT USAGE ON *.* TO 'arc'@localhost IDENTIFIED BY 'arc123';
/** 
    Grant all privileges to a user on a specific database
*/
GRANT ALL privileges ON `arc_project`.* TO 'arc'@localhost;

/* Apply Changes */
FLUSH PRIVILEGES;

/* Verify User */
SHOW GRANTS FOR 'arc'@localhost;